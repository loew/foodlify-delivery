import { NgModule }            from '@angular/core';
import { CommonModule }        from '@angular/common';
import { FilterPipe } from '../pipes/filter.pipe';
import { CuisinePipe } from '../pipes/cuisine.pipe';
import { NotZero } from '../pipes/not-zero.pipe';

@NgModule({
  imports:      [ CommonModule ],
  declarations: [ FilterPipe, CuisinePipe, NotZero ],
  exports:      [ FilterPipe, CuisinePipe, NotZero ]
})
export class SharedModule { }