import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BagComponent } from './components/bag/bag.component';
import { FeedComponent } from './components/feed/feed.component';


const routes: Routes = [
  {
    path: 'bag',
    component: BagComponent,
},
{
    path: '',
    component: FeedComponent,
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
