class FoodModel {
    id: String;
    title: String;
    price: String;
    cuisine: String;
}
export class FoodData {
    foodProperties: Array<FoodModel>;
}   