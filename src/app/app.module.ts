import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule }   from '@angular/forms';
import { SharedModule } from './shared/shared-module.module';
import { CommonModule } from '@angular/common';
import { BagComponent } from './components/bag/bag.component';
import { FeedComponent } from './components/feed/feed.component';
import { HttpClientModule } from '@angular/common/http'; 
import {NgxWebstorageModule} from 'ngx-webstorage';

@NgModule({
  declarations: [
    AppComponent,
    BagComponent,
    FeedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CommonModule,
    SharedModule,
    HttpClientModule,
    NgxWebstorageModule.forRoot()
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
