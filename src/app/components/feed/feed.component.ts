import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {

  ngOnInit(): void {
    this.getFoodList();
    this.assignCopy();
  }

  public title: string;
  public foods: any;
  public filteredFoods: any[];
  public selectedCuisine: string;
  public search: string;
  
  constructor(private appService : AppService) {
    this.appService.initLocalStorage();
    this.title = 'foodlify-delivery-app';
    this.foods = this.getFoodList();
    this.filteredFoods = [];
    this.selectedCuisine = "";
    this.search = "";
  }

  private getFoodList() {
    this.foods = this.appService.getFoods();
  }

  public incrementBag(food) {
    const dataObj = {
      "id": food.id,
      "title": food.title,
      "cuisine": food.cuisine,
      "price": food.price
    };
    this.appService.incrementBag(dataObj);
    this.appService.getBag();
  }

  public setCuisine(cuisine) {
    this.selectedCuisine = cuisine;
  }

  private assignCopy(){
    this.filteredFoods = Object.assign([], this.foods);
  }

}
