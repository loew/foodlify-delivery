import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-bag',
  templateUrl: './bag.component.html',
  styleUrls: ['./bag.component.scss']
})
export class BagComponent {

  public bag: any[];
  public amount: number;
  
  constructor(private appService : AppService) { 
    this.bag = this.appService.getBag();
    this.amount = this.getTotalAmount();
  }

  public getTotalAmount() {
    return this.appService.getAmount();
  }

  public incrementBag(item) {
    this.appService.incrementBag(item);
    this.bag = this.appService.getBag();
  }

  public removeFromBag(item) {
    this.appService.removeFromBag(item);
    this.bag = this.appService.getBag();
  }

}
