import { Pipe, PipeTransform } from '@angular/core';  

@Pipe({
  name: 'notZero'
})

export class NotZero implements PipeTransform {
  
  transform(value: any): any {
    return value.filter((v) => 
      parseInt(v.quantity) > 0
    )
  }

}