import { Pipe, PipeTransform } from '@angular/core';  

@Pipe({
  name: 'cuisine'
})

export class CuisinePipe implements PipeTransform {
  
  transform(value: any, selectedCuisine): any {
    if (!selectedCuisine) return value;
    return value.filter((v) => 
      v.cuisine.toLowerCase().indexOf(selectedCuisine.toLowerCase()) > -1
    )
  }

}